#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "user.h"

const time_t ctt = time(0);

QTcpServer* tcpServer = new QTcpServer();

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->connectBtn, SIGNAL(pressed()), this,  SLOT(slotConnect()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::slotConnect()
{
    int server_status = 0;
    if (!this->tcpServer.getConnection()){
        int port = ui->lineEdit_3->text().toInt();
        if (port < 1000 || port >= 10000){
           int n = QMessageBox::information(0,
                                            "Error",
                                            "Port number must be from 1000 to 10000");
        } else {
            this->tcpServer = server();
            QTcpServer* tcpServer = this->tcpServer.getServer();
            connect(tcpServer, SIGNAL(newConnection()), this, SLOT(newUser()));
            //if (!this->tcpServer.tryingToCreateConnection(port)){
            if (!tcpServer->listen(QHostAddress::Any, port) && server_status == 0){
                qDebug() << QObject::tr("Unable to start the server: %1.").arg(tcpServer->errorString());
            } else {
                server_status = 1;
                ui->connectBtn->setText(DISCONNECT);
                qDebug() << "Server was launched";
                this->tcpServer.setConnection(!this->tcpServer.getConnection());
                ui->listWidgetChat->clear();
                ui->listWidgetChat->addItem("Server was launched");
            }
        }
    } else {
        ui->connectBtn->setText(CONNECT);
        qDebug() << "Server was turned off";
        this->tcpServer.disconnectAllUsers();
        ui->listWidgetUsers->clear();
        qDebug() << this->tcpServer.getConnection();
        this->tcpServer.setConnection(!this->tcpServer.getConnection());
        ui->listWidgetChat->addItem("Server was turned off");
    }
}

void MainWindow::newUser()
{
    qDebug() << "New user!";
    QTcpSocket * tcpSocket = this->tcpServer.getServer()->nextPendingConnection();
    connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(slotReadyRead()), Qt::QueuedConnection);
    connect(tcpSocket, SIGNAL(disconnected()), this, SLOT(slotDisconnect()), Qt::QueuedConnection);
    //connect(tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(slotDisconnect()));
}

void MainWindow::addUserToServer(QTcpSocket *tcpSocket, QString name)
{
    if (this->tcpServer.isUserExist(name)){
        //user with this name already exist on the server
        map<QString, QString> mapToJson = {{"type", "loginStatus"},
                                           {"status", "nameDuplicated"}};
        QJsonDocument document = this->tcpServer.createJson(mapToJson);
        this->tcpServer.sendJsonToOne(tcpSocket, document);
    } else {
        user new_user = user(tcpSocket, name);
        this->tcpServer.addUserToRoom(MAIN_ROOM,new_user);
        this->tcpServer.addUser(new_user);
        map<QString, QString> mapToJson = {{"type", "loginStatus"},
                                           {"status", "success"}};
        QJsonDocument document = this->tcpServer.createJson(mapToJson);
        this->tcpServer.sendJsonToOne(tcpSocket, document);

        mapToJson = {{"type", "invitation"},
                     {"room", MAIN_ROOM}};
        document = this->tcpServer.createJson(mapToJson);
        this->tcpServer.sendJsonToOne(tcpSocket, document);

        mapToJson = {{"type", "userConnected"},
                     {"user", name},
                     {"room", MAIN_ROOM}};
        document = this->tcpServer.createJson(mapToJson);
        qDebug() << document.toJson();
        this->tcpServer.sendJsonToAll(document);
        vector<user> users = this->tcpServer.getUsers();
        for (int i = 0; i < users.size(); ++i){
            if (name != users[i].getName()){
                mapToJson = {{"type", "userConnected"},
                             {"user", users[i].getName() },
                             {"room", MAIN_ROOM}};
                this->tcpServer.sendJsonToOne(tcpSocket, this->tcpServer.createJson(mapToJson));
            }
        }
        ui->listWidgetUsers->addItem(name);
        ui->listWidgetChat->addItem(name + " connected to chat at " + QTime::currentTime().toString());
    }
}

void MainWindow::addMessageToRoom(QTcpSocket* socket, QString roomName,  QString message)
{
    QString name = this->tcpServer.findUserNameBySocket(socket);
    ui->listWidgetChat->addItem("(" + roomName + ")" + name + "(" + QTime::currentTime().toString() + ")" + ":" + message);
    this->tcpServer.addMessageToRoom(socket, roomName, message);
}

void MainWindow::addUserToRoom(QTcpSocket* socket, QString roomName){
    QString userName = this->tcpServer.findUserNameBySocket(socket);
    if (!this->tcpServer.isUserExist(userName)){
        return;
    }
    ui->listWidgetChat->addItem(userName + " connected to room:" + roomName + " at " + QTime::currentTime().toString());
    this->tcpServer.addUserToRoom(roomName, user(socket, userName));
}

void MainWindow::deleteUserFromRoom(QTcpSocket* socket, QString roomName){
    QString userName = this->tcpServer.findUserNameBySocket(socket);
    ui->listWidgetChat->addItem(userName + " disconnected from room: " + roomName + " at " + QTime::currentTime().toString());
    this->tcpServer.delUserFromRoom(roomName, user(socket, userName));
}

void MainWindow::inviteUserToRoom(QString roomName, QString userName){
    if (!this->tcpServer.isUserExist(userName)){
        return;
    }

    ui->listWidgetChat->addItem(userName + " was invited to room: " + roomName + " at " + QTime::currentTime().toString());
    this->tcpServer.inviteUserToRoom(roomName, userName);
}

void MainWindow::addRoomByUser(QTcpSocket *socket, room newRoom){
    if (this->tcpServer.isRoomExist(newRoom.getName())){
        return;
    }
    QString userName = this->tcpServer.findUserNameBySocket(socket);
    ui->listWidgetChat->addItem(userName + " added room: " + newRoom.getName() + " at " + QTime::currentTime().toString());
    this->tcpServer.addRoomByUser(socket, newRoom);
}

void MainWindow::slotReadyRead()
{
    qDebug() << "This is slot ReadyRead";
    QTcpSocket * socketRR = (QTcpSocket *)sender();
    QList<QByteArray> jsonDataArray = socketRR->readAll().split(char(1));
    for (auto it = jsonDataArray.begin(); it != jsonDataArray.end(); ++it){
        QByteArray jsonData = *it;
        qDebug() << jsonData;
        QJsonDocument document = QJsonDocument::fromJson(jsonData);
        QJsonObject object = document.object();
        QString type = object.value("type").toString();
        if (type == "login"){
            QString name = object.value("name").toString();
            qDebug() << (QString)name;
            this->addUserToServer(socketRR,  name);
        } else if (type == "msg"){
            QString msg = object.value("msg").toString();
            QString roomName = object.value("room").toString();
            qDebug() << (QString)roomName;
            this->addMessageToRoom(socketRR, roomName, msg);
        } else if (type == "comeInRoom"){
            QString roomName = object.value("room").toString();
            this->addUserToRoom(socketRR, roomName);
        } else if (type == "comeOutRoom"){
            QString roomName = object.value("room").toString();
            this->deleteUserFromRoom(socketRR, roomName);
        } else if (type == "invitation"){
            QString roomName = object.value("room").toString();
            QString userName = object.value("user").toString();
            qDebug() <<"before inviteUser";
            qDebug() << roomName;
            this->inviteUserToRoom(roomName, userName);
        } else if (type == "roomCreation"){
            qDebug() << jsonDataArray;
            QString roomName = object.value("room").toString();
            bool isFree = object.value("private").toString()=="true" ? false : true;
            qDebug() << isFree;
            this->addRoomByUser(socketRR, room(roomName, isFree));
            //this->tcpServer.addRoomByUser(socketRR, room(roomName, isFree));
        } else if (type == "roomListRequest"){
            this->tcpServer.sendRoomFreeList(socketRR);
        }
    }
}

void MainWindow::slotDisconnect(){
    qDebug() << "This is slotDisconnect";
    QTcpSocket* socketD = (QTcpSocket*)sender();
    socketD->disconnectFromHost();
    QString name = this->tcpServer.findUserNameBySocket(socketD);
    this->tcpServer.deleteUser(socketD);
    ui->listWidgetChat->addItem((QString)name + " disconnected from chat at " + QTime::currentTime().toString());
    vector<QString> users;
    for (int i = 0; i < ui->listWidgetUsers->count(); ++i){
        users.push_back(ui->listWidgetUsers->item(i)->text());
    }
    ui->listWidgetUsers->clear();
    for (int i = 0; i < users.size(); ++i){
        if (users[i] == name){
            users.erase(users.begin() + i);
            break;
        }
    }
    for (int i = 0; i < users.size(); ++i){
        ui->listWidgetUsers->addItem(users[i]);
    }

}
