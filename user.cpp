#include "user.h"

user::user(QTcpSocket* socket, QString name)
{
    this->name = name;
    this->socket = socket;
}

void user::setName(QString name){
    this->name = name;
}

QString user::getName(){
    return this->name;
}

void user::setSocket(QTcpSocket* socket){
    this->socket = socket;
}

QTcpSocket* user::getSocket(){
    return this->socket;
}
