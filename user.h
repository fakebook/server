#ifndef USER_H
#define USER_H

#include <QString>
#include <QTcpSocket>

class user
{
private:
    QString name;       //user name
    QTcpSocket* socket; //socket for user

public:
    user(QTcpSocket* socket, QString name);
    void setName(QString name);
    QString getName();
    void setSocket(QTcpSocket* socket);
    QTcpSocket* getSocket();
};

#endif // USER_H
