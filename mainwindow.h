#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpServer>
#include <QTcpSocket>
#include <QWidget>
#include <QByteArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QMessageBox>
#include <QTime>
#include <iostream>
#include <list>
#include <vector>
#include <ctime>
#include <string.h>
#include "server.h"

using namespace std;

const QString CONNECT = "Connect";
const QString DISCONNECT = "Disconnect";
const QString MAIN_ROOM = "Main Room";

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void addUserToServer(QTcpSocket* socket,  QString name);
    void addMessageToRoom(QTcpSocket* socket, QString roomName,  QString message);
    void addUserToRoom(QTcpSocket* socket, QString roomName);
    void deleteUserFromRoom(QTcpSocket* socket, QString roomName);
    void inviteUserToRoom(QString roomName, QString userName);
    void addRoomByUser(QTcpSocket* socket, room newRoom);

signals:
    void addMessageSignal(QTcpSocket* socket, QString message);
    void addUserSignal(QTcpSocket* socket, QString name);
    void delUserSignal(QTcpSocket * socket);

public slots:
    void slotConnect();
    void newUser();
    void slotReadyRead();
    void slotDisconnect();

private:
    Ui::MainWindow *ui;
    server tcpServer;
    int server_status;
};

#endif // MAINWINDOW_H
