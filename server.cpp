#include "server.h"

const QString MAIN_ROOM = "Main Room";

server::server()
{
    this->connection = false;
    this->tcpServer = new QTcpServer();
    this->rooms.push_back(room(MAIN_ROOM, true));
}

bool server::getConnection(){
    return this->connection;
}

void server::setConnection(bool connection){
    this->connection = connection;
}

QTcpServer* server::getServer(){
    return this->tcpServer;
}

void server::setServer(QTcpServer* tcpServer){
    this->tcpServer = tcpServer;
}

vector<user> server::getUsers(){
    return this->users;
}

void server::setUsers(vector<user> users){
    this->users = users;
}

int server::getServerStatus(){
    return this->serverStatus;
}

void server::setServerStatus(int serverStatus){
    this->serverStatus = serverStatus;
}

void server::addUser(user newUser){
    this->users.push_back(newUser);
}

bool server::isUserExist(QString name){
    for (int i = 0; i < (int)users.size(); ++i){
        if (users[i].getName() == name){
            return true;
        }
    }
    return false;
}

void server::deleteUser(QTcpSocket* socket){
    for (int i = 0; i < (int)this->users.size(); ++i){
        if (this->users[i].getSocket() == socket){
            this->users.erase(this->users.begin() + i);
            break;
        }
    }
    for (int i = 0; i < (int)this->rooms.size(); ++i){
        this->rooms[i].deleteUser(socket);
    }
    for (int i = 0; i < (int)rooms.size(); ++i){
        if(this->rooms[i].isEmpty() && this->rooms[i].getName() != MAIN_ROOM){
            this->rooms.erase(this->rooms.begin()+ i);
            -- i;
        }
    }
}

QString server::findUserNameBySocket(QTcpSocket *socket){
    for (int i = 0; i< (int)this->users.size(); ++i){
        if (this->users[i].getSocket() == socket){
            return this->users[i].getName();
        }
    }
    return "";
}

QJsonDocument server::createJson(const  map<QString, QString>&message){
    QJsonObject newJson;
    for (const auto& item: message){
        newJson.insert(item.first, QJsonValue::fromVariant(item.second));
    }
    QJsonDocument doc(newJson);
    return doc;
}

void server::disconnectAllUsers(){
    this->getServer()->close();
    while (!this->users.empty()){
        this->users[0].getSocket()->disconnectFromHost();
        this->users.erase(this->users.begin());
    }
}

void server::addMessageToRoom(QTcpSocket *socket, QString roomName, QString message){
    room currentRoom = this->findRoomByName(roomName);
    QString name = this->findUserNameBySocket(socket);
    map<QString, QString> mapToJson = {{"type", "msg"},
                                       {"user", name},
                                       {"msg", message},
                                       {"room", roomName}};
    currentRoom.sendJsonToAll(this->createJson(mapToJson));
}

void server::sendJsonToAll(QJsonDocument doc){
    for (int i = 0; i< this->users.size(); ++i){
        qDebug() << this->users[i].getName();
        this->users[i].getSocket()->write(doc.toJson().append(char(1)));
    }
}

void server::sendJsonToOne(QTcpSocket* tcpSocket, QJsonDocument doc){
    tcpSocket->write(doc.toJson().append(1));
}

void server::addRoomByUser(QTcpSocket* socket, room newRoom){
    QString roomName = newRoom.getName();
    if (this->isRoomExist(roomName)){
        map<QString, QString> mapToJson = {{"type", "roomCreationStatus"},
                                           {"room", roomName},
                                           {"status", "nameDuplicated"}};
        this->sendJsonToOne(socket, this->createJson(mapToJson));
    } else {
        map<QString, QString> mapToJson = {{"type", "roomCreationStatus"},
                                           {"room", roomName},
                                           {"status", "success"}};
        this->sendJsonToOne(socket, this->createJson(mapToJson));
        qDebug() << "this is addRoomByUser";
        qDebug() << this->createJson(mapToJson).toJson();
        user firstUser = this->findUserBySocket(socket);
        this->rooms.push_back(newRoom);
        this->inviteUserToRoom(roomName, firstUser.getName());
    }
}

void server::addUserToRoom(QString roomName, user newUser){
    room currentRoom = this->findRoomByName(roomName);
    vector<user> users = currentRoom.getUsers();
    for (int i = 0; i < users.size(); ++i){
        if (newUser.getName() != users[i].getName()){
            map<QString, QString> mapToJson = {{"type", "userConnected"},
                                               {"user", users[i].getName() },
                                               {"room", roomName}};
            this->sendJsonToOne(newUser.getSocket(), this->createJson(mapToJson));
            mapToJson = {{"type", "userConnected"},
                         {"user", newUser.getName()},
                         {"room", roomName}};
            this->sendJsonToOne(users[i].getSocket(), this->createJson(mapToJson));
        }
    }
    for (int i = 0; i< (int)this->rooms.size(); ++i){
        if (this->rooms[i].getName() == roomName){
            this->rooms[i].addUser(newUser);
            return;
        }
    }
}

void server::delUserFromRoom(QString roomName, user oldUser){
    for (int i = 0; i < (int)this->rooms.size(); ++i){
        if (this->rooms[i].getName() == roomName){
            this->rooms[i].deleteUser(oldUser.getSocket());
            map<QString, QString> mapToJson = {{"type", "userDisconnected"},
                                              {"room", roomName},
                                              {"user", oldUser.getName()}};
            QJsonDocument doc = this->createJson(mapToJson);
            vector<user> users = this->rooms[i].getUsers();
            for (int i = 0; i < users.size(); ++i){
                this->sendJsonToOne(users[i].getSocket(), doc);
            }
            if (this->rooms[i].isEmpty() && this->rooms[i].getName() != MAIN_ROOM){
                this->rooms.erase(this->rooms.begin() + i);
            }
            return;
        }
    }
}

user server::findUserBySocket(QTcpSocket *socket){
    for (int i = 0; i < (int)this->users.size(); ++i){
        if (this->users[i].getSocket() == socket){
            return this->users[i];
        }
    }
}

bool server::tryingToCreateConnection(int port){
    return this->tcpServer->listen(QHostAddress::Any, port) && this->getServerStatus() == 0;
}

void server::inviteUserToRoom(QString roomName, QString userName){

    if (!this->isUserExist(userName)){
        return;
    }
    user newUser = this->findUserByUserName(userName);
    for (int i = 0; i < (int)this->rooms.size(); ++i){
        if (this->rooms[i].getName() == roomName){
            this->rooms[i].addUser(newUser);
            break;
        }
    }
    map<QString, QString> mapToJson = {{"type", "invitation"},
                 {"room", roomName}};
    qDebug() << "This is inviteUserToRoom";
    qDebug() << mapToJson;
    this->sendJsonToOne(newUser.getSocket(), this->createJson(mapToJson));

    room currentRoom = this->findRoomByName(roomName);
    vector<user> users = currentRoom.getUsers();
    QJsonDocument doc = this->createJson(mapToJson);
    for (int i = 0; i < users.size(); ++i){
        mapToJson = {{"type", "userConnected"},
                     {"room", roomName},
                     {"user", users[i].getName()}};
        QJsonDocument doc = createJson(mapToJson);
        for (int j = 0; j < users.size(); ++j){
            this->sendJsonToOne(users[j].getSocket(), doc);
        }
    }
}

void server::sendRoomFreeList(QTcpSocket* socket){
    QStringList res;
    qDebug() << "this is rooms size";
    qDebug() << rooms.size();
    for (int i = 0; i < this->rooms.size(); ++i){
        if (this->rooms[i].isFreeComing()){
            res.append(this->rooms[i].getName());
        }
    }
    QJsonObject msgResponse;
    msgResponse.insert("type", QJsonValue::fromVariant("roomList"));
    msgResponse.insert("rooms", QJsonValue::fromVariant(res));
    QJsonDocument doc(msgResponse);
    qDebug() << doc.toJson();
    socket->write(doc.toJson().append(char(1)));
}

room server::findRoomByName(QString name){
    for (int i = 0; i < this->rooms.size(); ++i){
        if (this->rooms[i].getName() == name){
            return this->rooms[i];
        }
    }
}

bool server::isRoomExist(QString roomName){
    for (int i = 0; i < (int)this->rooms.size(); ++i){
        if (this->rooms[i].getName() == roomName){
            return true;
        }
    }
    return false;
}

user server::findUserByUserName(QString userName){
    vector<user> users = this->getUsers();
    for (int i = 0; i < users.size(); ++i){
        if (users[i].getName() == userName){
            return users[i];
        }
    }
}

bool server::isUserInRoom(QString roomName, QString userName){
    room newRoom = this->findRoomByName(roomName);
    return newRoom.isUserExist(userName);
}
