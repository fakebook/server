#ifndef ROOM_H
#define ROOM_H

#include <vector>
#include <QJsonDocument>
#include "user.h"

using namespace std;

class room
{
private:
    QString name;       //name of room
    vector<user> users; //users in room
    bool freeComing;    //indicator for private room

public:
    room(QString name, bool freeComing);
    QString getName();
    void setName(QString name);
    vector<user> getUsers();
    void setUsers(vector<user> users);
    void addUser(user newUser);                 //add new user
    void deleteUser(QTcpSocket* socket);        //delete user
    bool isEmpty();                             //check room is empty
    bool isFreeComing();                        //check room for private
    void setFreeComing(bool freeComing);
    void sendJsonToAll(QJsonDocument document); //send json message for all users in room
    bool isUserExist(QString userName);         //check user in this room

};

#endif // ROOM_H
