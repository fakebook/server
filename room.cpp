#include "room.h"

room::room(QString name, bool freeComing)
{
    this->name = name;
    this->freeComing = freeComing;
}

QString room::getName(){
    return this->name;
}

void room::setName(QString name){
    this->name = name;
}

vector<user> room::getUsers(){
    return this->users;
}

void room::setUsers(vector<user> users){
    this->users = users;
}

void room::addUser(user newUser){
    this->users.push_back(newUser);
}

void room::deleteUser(QTcpSocket* socket){
    for(int i = 0; i < (int)this->users.size(); ++i){
        if (this->users[i].getSocket() == socket){
            this->users.erase(this->users.begin() + i);
            return;
        }
    }
}

bool room::isEmpty(){
    return this->users.empty();
}

bool room::isFreeComing(){
    return this->freeComing;
}

void room::setFreeComing(bool freeComing){
    this->freeComing = freeComing;
}

void room::sendJsonToAll(QJsonDocument document){
    for (int i = 0; i< this->users.size(); ++i){
        this->users[i].getSocket()->write(document.toJson().append(char(1)));
    }
}

bool room::isUserExist(QString userName){
    vector<user> users = this->getUsers();
    for (int i = 0; i < (int)users.size(); ++i){
        if (users[i].getName() == userName){
            return true;
        }
    }
    return false;
}
