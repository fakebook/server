#ifndef MYSERVER_H
#define MYSERVER_H

class MyServer: public QDialog{
    Q_OBJECT

public :
    explicit MyServer(QWidget *parent = nullptr);

private slots:
    void sessionOpened();
    void sedFortune();

private:
    QLabel *statusLabel = nullptr;
    QTcpServer *tcpServer = nullptr;
    QVector<QString> fotrunes;
    QNetworkSession *newtworkSession = nullptr;

};


#endif // MYSERVER_H
