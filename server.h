#ifndef SERVER_H
#define SERVER_H

#include <QTcpServer>
#include <QJsonObject>
#include <QJsonDocument>
#include <vector>
#include "user.h"
#include "room.h"

using namespace std;

class server
{
private:
    bool connection;        //indicator connection
    QTcpServer* tcpServer;  //pointer to server
    vector<user> users;     //users on this server
    vector<room> rooms;     //rooms on this server
    int serverStatus;       //indicator for server

public:
    server();
    bool getConnection();
    void setConnection(bool connection);
    QTcpServer* getServer();
    void setServer(QTcpServer* tcpServer);
    vector<user> getUsers();
    void setUsers(vector<user> users);
    int getServerStatus();
    void setServerStatus(int serverStatus);
    bool isUserExist(QString name);
    void addUser(user newUser);                                                     //add user on server
    void deleteUser(QTcpSocket* socket);                                            //delete user from server
    QString findUserNameBySocket(QTcpSocket* socket);                               //find user name by socket
    QJsonDocument createJson(const  map<QString, QString>&message);                 //create json from map
    void disconnectAllUsers();                                                      //disconnect all users
    void addMessageToRoom(QTcpSocket* socket, QString roomName, QString message);   //add message to room
    void sendJsonToAll(QJsonDocument doc);                                          //send json to all users
    void sendJsonToOne(QTcpSocket* socket, QJsonDocument doc);                      //send json to one user
    void addRoomByUser(QTcpSocket* socket, room newRoom);                           //add room to server
    void addUserToRoom(QString roomName, user newUser);                             //add user to room
    void delUserFromRoom(QString roomName, user oldUser);                           //delete user from room
    user findUserBySocket(QTcpSocket* socket);                                      //find user on server
    bool tryingToCreateConnection(int port);                                        //try to get connection on port
    void inviteUserToRoom(QString roomName, QString userName);                      //invite user to room
    void sendRoomFreeList(QTcpSocket* socket);                                      //send list non private rooms
    room findRoomByName(QString name);                                              //find user by name
    bool isRoomExist(QString roomName);                                             //check room on server
    user findUserByUserName(QString userName);                                      //find user by name
    bool isUserInRoom(QString roomName, QString userName);                          //check user in room

public slots:
    void addMessageSignal(QTcpSocket* tcpSocket, QString message);
    void addUserSignal(QTcpSocket* tcpSocket, QString name);
    void delUserSignal(QTcpSocket* tcpSocket);

};

#endif // SERVER_H
