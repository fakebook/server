#-------------------------------------------------
#
# Project created by QtCreator 2018-01-17T14:34:55
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Chat
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    user.cpp \
    server.cpp \
    room.cpp

HEADERS  += mainwindow.h \
    user.h \
    server.h \
    room.h

FORMS    += mainwindow.ui
